/* CODE STRUCTURE */
/***************
 * index.js control all of them
 * helper.js 
 * ./controller/homeController.js
 *  - ./model/Home.js control data
 *      + Get data from api and add data to localStorage
 *      + Read data from localStorage
 *      + Add new product to api
 *  - ./view/homeView.js render data home page and pagination
 * - ./model/formValid.js handle input from form
 *  + add new product
 * product 
 */
import { clearInput } from './view/base.js'
import { homeController } from './controller/homeController.js'
import { editController } from './controller/productControl.js'
import * as formValid from './model/formValid.js'


const init = async () => {
    /* HOME CONTROLLER */
    await homeController();
    // add product button
    document.querySelector(".addButton").addEventListener('click',  ()=> {
        // Clear input form value
        clearInput();
        // Add button add new product
        document.getElementsByClassName("modal-footer")[0].innerHTML = `
        <button class="btn btn-success" onclick="addController(event)" >Thêm</button>
        `;
        //data-dismiss="modal"
        // Check valid input when user change value
        formValid.checkInputValue();
    })
    document.getElementById('result').addEventListener('click', (e)=>{
        const currentID = e.target.closest('tr').dataset.productid;
        if (e.target.matches('.edit, .edit *')){
            editController(currentID);
        }
        else (e.target.matches('.delete, .delete *'))
        {
             // Delete
             document.getElementsByClassName("modal-footer")[1].innerHTML = `
        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
        <button class="btn btn-danger" onclick="deteleProductController(${currentID});" data-dismiss="modal">Xác nhận</button>
        `;
            
        }
    })
    
}

// Khởi tạo chương trình 
init();
// (new Date()).getTime()
// aiosHelper.postProduct({
//     id: 200,
//     name: "phone",
//     image: "https://cdn.tgdd.vn/Products/Images/42/225176/realme-c3-64gb-263620-023637-200x200.jpg",
//     description: "description 5",
//     price:3.39,
//     inventory:123,
//     rating:312, 
//     type:"xiaomi",
// })