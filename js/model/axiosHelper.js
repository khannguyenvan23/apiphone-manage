
// Get Data
const  url = "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products";
// const url = "https://5f2a9bd76ae5cc0016422c69.mockapi.io/product";
export const getData = async ()=>{
    try {
        const response = await axios.get(url);
        return response.data;
      } catch (error) {
        console.error("Ko lay dc du lieu");
      }
}


// Send a POST request

export const postProduct = async (product)=>{
    try {
        await axios({
            method: 'post',
            url: url,
            data: product
          });
      } catch (error) {
        console.error(error);
      }
}
export const getProduct = async (id)=>{
  try {
    const response =  await axios({
          method: 'get',
          url: url + `/${id}`
        });
      return response.data;
    } catch (error) {
      console.error(error);
    }
}
export const putProduct = async (data, id)=>{
  try {
      await axios({
          method: 'put',
          url: url + `/${id}`,
          data: data
        });
    } catch (error) {
      console.error(error);
    }
}
export const delProduct = async (id)=>{
  try {
      await axios({
          method: 'delete',
          url: url + `/${id}`
        });
      console.log("delte thanh cong");
    } catch (error) {
      console.error(error);
    }
}

// postData({
//     description: 'https://media0.giphy.com/media/MdA16VIoXKKxNE8Stk/giphy.gif?cid=ecf05e47tw5mky3lf3c2lq5gx4nxw986hpk9sk8dsfw0sayd&rid=giphy.gif'
//   })
// putData({description: "pút không dc"}, 21);
