import {inputForm, inputError } from '../view/base.js'
export const checkWhenCLickEvent = () => {
    
    let flag = 1;
    // if the email field is valid, we let the form submit
        if(!inputForm.nameSelected.validity.valid) {
            // If it isn't, we display an appropriate error message
            showErrorName(inputForm.nameSelected, inputError.errorName);
            flag = -1;
            // Then we prevent the form from being sent by canceling the event
            // event.preventDefault();
        }
        if(!inputForm.imageSelected.validity.valid) {
            // If it isn't, we display an appropriate error message
            showErrorImage(inputForm.imageSelected, inputError.errorImage);
            flag = -1;
            // Then we prevent the form from being sent by canceling the event
            // event.preventDefault();
        }
        if(!inputForm.descriptionSelected.validity.valid) {
            // If it isn't, we display an appropriate error message
            showErrorDescription(inputForm.descriptionSelected, inputError.errorDescription);
            flag = -1;
            // Then we prevent the form from being sent by canceling the event
            // event.preventDefault();
        }
        if(!inputForm.priceSelected.validity.valid) {
            // If it isn't, we display an appropriate error message
            showErrorPrice(inputForm.priceSelected, inputError.errorPrice);
            flag = -1;
            // Then we prevent the form from being sent by canceling the event
            // event.preventDefault();
        }
        if(!inputForm.inventorySelected.validity.valid) {
            // If it isn't, we display an appropriate error message
            showErrorInventory(inputForm.inventorySelected, inputError.errorInventory);
            flag = -1;
            // Then we prevent the form from being sent by canceling the event
            // event.preventDefault();
        }
        if(!inputForm.ratingSelected.validity.valid) {
            // If it isn't, we display an appropriate error message
            showErrorInventory(inputForm.ratingSelected, inputError.errorRating);
            flag = -1;
            // Then we prevent the form from being sent by canceling the event
            // event.preventDefault();
        }
        if(inputForm.typePhoneSelected.value === "") {
            // If it isn't, we display an appropriate error message
            showErrorTypePhone(inputForm.typePhoneSelected, inputError.errorTypePhone);
            flag = -1;
            // Then we prevent the form from being sent by canceling the event
            // event.preventDefault();
        }
        if(flag === -1){
            return false;
        }
        return true;
       
};
export const checkInputValue = () => {
    // Check valid product
            $input(inputForm.nameSelected, inputError.errorName, showErrorName);
        
            $input(inputForm.imageSelected, inputError.errorImage, showErrorImage);
    
            $input(inputForm.descriptionSelected, inputError.errorDescription, showErrorDescription);
    
            $input(inputForm.priceSelected, inputError.errorPrice, showErrorPrice);
            
            $input(inputForm.inventorySelected, inputError.errorInventory, showErrorInventory);
    
            $input(inputForm.ratingSelected, inputError.errorRating, showErrorRating);
            
            $change(inputForm.typePhoneSelected, inputError.errorTypePhone, showErrorTypePhone);
}

// select Form valid
export const $input = (target, selectedError, callback) => {
    target.addEventListener('input', function (event) {
        // Each time the user types something, we check if the
        // form fields are valid.

        if (target.validity.valid) {
            // In case there is an error message visible, if the field
            // is valid, we remove the error message.
            selectedError.innerHTML = ''; // Reset the content of the message
            selectedError.className = 'error'; // Reset the visual state of the message
        } else {
            // If there is still an error, show the correct error
            callback(target, selectedError);
        }
        });
}
export const $change = (target, selectedError, callback) => {
    target.addEventListener('change', function (event) {
        // Each time the user types something, we check if the
        // form fields are valid.
        if (target.value !== "") {
            // In case there is an error message visible, if the field
            // is valid, we remove the error message.
            selectedError.innerHTML = ''; // Reset the content of the message
            selectedError.className = 'error'; // Reset the visual state of the message
        } else {
            // If there is still an error, show the correct error
            callback(target, selectedError);

        }
        });
}

// Show error message

export const showErrorName = (target, selectedError) => {
    if(target.validity.valueMissing) {
        // If the field is empty
        // display the following error message.
        selectedError.textContent = 'Bạn cần nhập tên điện thoại.';
    } else if(target.validity.patternMismatch) {
        // If the field doesn't contain an email address
        // display the following error message.
        selectedError.textContent = 'Bạn cần nhập đúng định dạng không có ký tự đặc biệt';
    } else if(target.validity.tooShort) {
        // If the data is too short
        // display the following error message.
        selectedError.textContent = `Tên điện thoại có độ dài ít nhất là ${ target.minLength } ký tự; bạn nhập ${ target.value.length }.`;
    }
    // Set the styling appropriately
    selectedError.className = 'error active';
}
export const showErrorImage = (target, selectedError) => {
    if(target.validity.valueMissing) {
        // If the field is empty
        // display the following error message.
        selectedError.textContent = 'Bạn cần nhập url hình điện thoại.';
    } else if(target.validity.patternMismatch) {
        // If the field doesn't contain an email address
        // display the following error message.
        selectedError.textContent = 'Bạn cần nhập đúng định dạng url hình ảnh';
    } else if(target.validity.tooShort) {
        // If the data is too short
        // display the following error message.
        selectedError.textContent = `Url ảnh có độ dài ít nhất là ${ target.minLength } ký tự; bạn nhập ${ target.value.length }.`;
    }
    // Set the styling appropriately
    selectedError.className = 'error active';
}
export const showErrorDescription = (target, selectedError) => {
    if(target.validity.valueMissing) {
        // If the field is empty
        // display the following error message.
        selectedError.textContent = 'Bạn cần nhập mô tả.';
    } else if(target.validity.patternMismatch) {
        // If the field doesn't contain an email address
        // display the following error message.
        selectedError.textContent = 'Bạn cần nhập đúng định dạng hình ảnh';
    } else if(target.validity.tooShort) {
        // If the data is too short
        // display the following error message.
        selectedError.textContent = `Mô tả có độ dài ít nhất là ${ target.minLength } ký tự; bạn nhập ${ target.value.length }.`;
    }
    // Set the styling appropriately
    selectedError.className = 'error active';
}

export const showErrorPrice = (target, selectedError) => {
    if(target.validity.valueMissing) {
        // If the field is empty
        // display the following error message.
        selectedError.textContent = 'Bạn cần nhập giá tiền điện thoại.';
    } else if(target.validity.patternMismatch) {
        // If the field doesn't contain an email address
        // display the following error message.
        selectedError.textContent = 'Bạn cần nhập đúng định dạng giá tiền';
    } else if(target.validity.tooShort) {
        // If the data is too short
        // display the following error message.
        selectedError.textContent = `Giá tiền trăm ngàn trở lên có độ dài ít nhất là ${ price.minLength } ký tự; bạn nhập ${ target.value.length }.`;
    }
    // Set the styling appropriately
    selectedError.className = 'error active';
}
export const showErrorInventory = (target, selectedError) => {
    if(target.validity.valueMissing) {
        // If the field is empty
        // display the following error message.
        selectedError.textContent = 'Bạn cần nhập số lượng điện thoại.';
    } else if(target.validity.patternMismatch) {
        // If the field doesn't contain an email address
        // display the following error message.
        selectedError.textContent = 'Bạn cần nhập đúng định số lượng';
    } else if(target.validity.tooShort) {
        // If the data is too short
        // display the following error message.
        selectedError.textContent = `Số lượng có độ dài ít nhất là ${ target.minLength } ký tự; bạn nhập ${ target.value.length }.`;
    }
    // Set the styling appropriately
    selectedError.className = 'error active';
}
export const showErrorRating = (target, selectedError) => {
    if(target.validity.valueMissing) {
        // If the field is empty
        // display the following error message.
        selectedError.textContent = 'Bạn cần nhập điểm số đánh giá điện thoại.';
    } else if(target.validity.patternMismatch) {
        // If the field doesn't contain an email address
        // display the following error message.
        selectedError.textContent = 'Bạn cần nhập đúng định số lượng';
    } else if(target.validity.tooShort) {
        // If the data is too short
        // display the following error message.
        selectedError.textContent = `Số lượng có độ dài ít nhất là ${ target.minLength } ký tự; bạn nhập ${ target.value.length }.`;
    }
    // Set the styling appropriately
    selectedError.className = 'error active';
}
export const showErrorTypePhone = (target, selectedError) => {
    if(target.value === "") {
        // If the field is empty
        // display the following error message.
        console.log("hello work ko")
        selectedError.textContent = 'Bạn cần chọn loại điện thoại.';
    }
    // Set the styling appropriately
    selectedError.className = 'error active';
}








