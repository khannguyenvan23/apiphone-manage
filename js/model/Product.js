import * as axiosHelper from './axiosHelper.js'
import { inputForm } from '../view/base.js'
export default class Product {
    // {id, name, image, description, price, inventory, rating, type}
    constructor(){
       
    }
    async getProduct(id) {
        const product = await axiosHelper.getProduct(id);
        this.id = product.id;
        this.name = product.name;
        this.image = product.image;
        this.description = product.description;
        this.price = product.price;
        this.inventory = product.inventory;
        this.rating = product.rating;
        this.type = product.type;
    }
    fillForm(){
        inputForm.nameSelected.value = this.name;
        inputForm.imageSelected.value = this.image;
        inputForm.descriptionSelected.value = this.description;
        inputForm.priceSelected.value =this.price;
        inputForm.inventorySelected.value = this.inventory;
        inputForm.ratingSelected.value = this.rating;
    }
    async updateAPI(id){
        this.name = inputForm.nameSelected.value;
        this.image = inputForm.imageSelected.value;
        this.description = inputForm.descriptionSelected.value;
        this.price = parseFloat(inputForm.priceSelected.value);
        this.inventory = parseFloat(inputForm.inventorySelected.value);
        this.rating = parseFloat(inputForm.ratingSelected.value);
        this.type = inputForm.typePhoneSelected.value
       
        await axiosHelper.putProduct( {
            name: this.name,
            image: this.image,
            description:this.description,
            price:this.price,
            inventory:this.inventory,
            rating:this.rating, 
            type:this.type
        }, id);
    }
    async addNewProduct() {
        this.id = (new Date()).getTime();
        this.name = inputForm.nameSelected.value;
        this.image = inputForm.imageSelected.value;
        this.description = inputForm.descriptionSelected.value;
        this.price = parseFloat(inputForm.priceSelected.value);
        this.inventory = parseFloat(inputForm.inventorySelected.value);
        this.rating = parseFloat(inputForm.ratingSelected.value);
        this.type = inputForm.typePhoneSelected.value
       const data = {
            id: this.id,
            name: this.name,
            image: this.image,
            description:this.description,
            price:this.price,
            inventory:this.inventory,
            rating:this.rating, 
            type:this.type
       }
        await axiosHelper.postProduct(data);
    }
    static async deleteProduct(id){
        await axiosHelper.delProduct(id);
    }
    
}