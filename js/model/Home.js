import * as axiosHelper from './axiosHelper.js'
export default class Home {
    
    constructor(){
        this.Data = [];
    }
    async getData(){
        // Get data from API
        // Create new obj push all API.data = new obj
        // Push obj to Data and return this data
        const data = await axiosHelper.getData();
        this.Data = data; 
        return this.Data; 
    }
    static findPage(thispages){
        let pages = thispages === "add" ?  Math.ceil(result.length / 5):thispages === "edit" || thispages === "del" ? parseInt(window.location.hash.replace('#', '')) || 1: 1;
        if(thispages === "del"){
            if(result.length % 10 === 0 && result.length > 0){
                pages = pages - 1
            }else if(result.length === 0) {
                alert("Ban khong con san pham nao");
            }
            return pages;
        }
        return pages;
    }
    saveLocal(){
        localStorage.setItem('productData', JSON.stringify(this.Data))
    }
    readLocal(){
        const storage = JSON.parse(localStorage.getItem('productData'))
        if(storage) this.Data = storage;
    }
    findIndex(id){
        return this.Data.findIndex(el =>el.id === id);
    }
    // updateProductData(product, id){
    //     const index = this.findIndex(id);
    //     this.Data.splice(index, 1, product);;
    //     this.saveLocal();
    // }
    // removeProduct(id){
    //     const index = this.findIndex(id);
    //     this.Data.splice(index, 1);
    // }
}