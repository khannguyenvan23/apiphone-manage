import Home from '../model/Home.js'
import * as homeView from '../view/homeView.js'

import {loadingUI,  clearLoadingUI} from '../view/base.js';

/**
 * Controller model/Home + view/homeView
 * - When chrome.load state.data = LocalStorage
 * - Clear Result UI
 * - Add Loading UI
 * - Get Data product phone from API Save to state.data and save LocalStorage
 * - Render Data To UI
 * - Clear Loading UI 
 * - Delete ALl Product
 * - Add Pages pagination 5 products each pages
 * */
let state = {};
export const homeController = async (thispages) =>{
    if(!state.home) state.home = new Home();
    homeView.clearResult();
        // Clear Result UI
       
        // Add Loading UI When waiting for API res
        loadingUI();
        // Get Data product phone from API 
   
    try {
        let result = await state.home.getData();
       // save LocalStorage and read when nothing inside
        if(result){
            state.home.saveLocal();
        }else {
            state.home.readLocal();
            result = state.home.Data;
        }
        // Render Data To UI
       
        let pages = thispages === "add" ?  Math.ceil(result.length / 5):thispages === "edit" || thispages === "del" ? parseInt(window.location.hash.replace('#', '')) || 1: 1;
        // If result 50 % 10 

        homeView.renderResults(result, pages);
        // Clear loading when finish render
        
            clearLoadingUI();
    
       
    } catch (error) {
        console.log(error)
    }
   
}
//Add Pages pagination 5 products each pages
window.pageController = (btn) => {
    if (btn) {
        const goToPage = parseInt(btn, 10);
        homeView.clearResult();
        homeView.renderResults(state.home.Data,  goToPage);
    }
}
// Add Pages pagination 5 products each pages
// export const pageController = () => {
//     const btn = window.location.hash.replace('#', '');
//     if (btn) {
//          state.goToPage = parseInt(btn);
//         homeView.clearResult();
//         homeView.renderResults(state.home.Data,  state.goToPage);
//     }
// }


// // Handle value from input
// const handleProduct = (target, callback, currentID) => {
// // Check valid product
//         formValid.$input(inputForm.nameSelected, inputError.errorName, formValid.showErrorName);
    
//         formValid.$input(inputForm.imageSelected, inputError.errorImage, formValid.showErrorImage);

//         formValid.$input(inputForm.descriptionSelected, inputError.errorDescription, formValid.showErrorDescription);

//         formValid.$input(inputForm.priceSelected, inputError.errorPrice, formValid.showErrorPrice);
        
//         formValid.$input(inputForm.inventorySelected, inputError.errorInventory, formValid.showErrorPrice);

//         formValid.$input(inputForm.ratingSelected, inputError.errorRating, formValid.showErrorRating);
        
//         formValid.$change(inputForm.typePhoneSelected, inputError.errorTypePhone, formValid.showErrorTypePhone);
//         // When user click 
//         target.addEventListener('click',  () => {
//         // if the email field is valid, we let the form submit
//             let flag = 1;
//             if(!inputForm.nameSelected.validity.valid) {
//                 // If it isn't, we display an appropriate error message
//                 formValid.showErrorName(inputForm.nameSelected, inputError.errorName);
//                 flag = -1;
//                 // Then we prevent the form from being sent by canceling the event
//                 // event.preventDefault();
//             }
//             if(!inputForm.imageSelected.validity.valid) {
//                 // If it isn't, we display an appropriate error message
//                 formValid.showErrorImage(inputForm.imageSelected, inputError.errorImage);
//                 flag = -1;
//                 // Then we prevent the form from being sent by canceling the event
//                 // event.preventDefault();
//             }
//             if(!inputForm.descriptionSelected.validity.valid) {
//                 // If it isn't, we display an appropriate error message
//                 formValid.showErrorDescription(inputForm.descriptionSelected, inputError.errorDescription);
//                 flag = -1;
//                 // Then we prevent the form from being sent by canceling the event
//                 // event.preventDefault();
//             }
//             if(!inputForm.priceSelected.validity.valid) {
//                 // If it isn't, we display an appropriate error message
//                 formValid.showErrorPrice(inputForm.priceSelected, inputError.errorPrice);
//                 flag = -1;
//                 // Then we prevent the form from being sent by canceling the event
//                 // event.preventDefault();
//             }
//             if(!inputForm.inventorySelected.validity.valid) {
//                 // If it isn't, we display an appropriate error message
//                 formValid.showErrorInventory(inputForm.inventorySelected, inputError.errorInventory);
//                 flag = -1;
//                 // Then we prevent the form from being sent by canceling the event
//                 // event.preventDefault();
//             }
//             if(!inputForm.ratingSelected.validity.valid) {
//                 // If it isn't, we display an appropriate error message
//                 formValid.showErrorInventory(inputForm.ratingSelected, inputError.errorRating);
//                 flag = -1;
//                 // Then we prevent the form from being sent by canceling the event
//                 // event.preventDefault();
//             }
//             if(inputForm.typePhoneSelected.value === "") {
//                 // If it isn't, we display an appropriate error message
//                 formValid.showErrorTypePhone(inputForm.typePhoneSelected, inputError.errorTypePhone);
//                 flag = -1;
//                 // Then we prevent the form from being sent by canceling the event
//                 // event.preventDefault();
//             }
//             if(flag === 1){
//                 callback(currentID);  
//             }
           
//         });
// }

// // Add new product
// const addExecute = async () => {
//      // Making obj base on input
//      state.product.id = (new Date()).getTime();
//      state.product.name = inputForm.nameSelected.value;
//      state.product.image = inputForm.imageSelected.value;
//      state.product.description = inputForm.descriptionSelected.value;
//      state.product.price = +inputForm.priceSelected.value;
//      state.product.inventory = +inputForm.inventorySelected.value;
//      state.product.rating = +inputForm.ratingSelected.value;
//      state.product.type = inputForm.typePhoneSelected.value
//      await state.home.addNewProduct(state.product);
//      clearInput();
//      homeView.clearResult();
//      // Add Loading UI When waiting for API res
//      // loadingUI();
//          // Render Data To UI
//      homeView.renderResults(state.home.Data,state.goToPage);
//          // Clear loading when finish render
//      // clearLoadingUI();
     
//      alert("Them thanh cong")
// }
// const editExecute = async (currentID) => {
//     if(currentID){
//         state.product.name = inputForm.nameSelected.value;
//         state.product.image = inputForm.imageSelected.value;
//         state.product.description = inputForm.descriptionSelected.value;
//         state.product.price = parseFloat(inputForm.priceSelected.value);
//         state.product.inventory = parseFloat(inputForm.inventorySelected.value);
//         state.product.rating = parseFloat(inputForm.ratingSelected.value);
//         state.product.type = inputForm.typePhoneSelected.value
        
        
//         state.home.updateProductData(state.product, currentID);
//         // reset input
       
        
//         homeView.clearResult();
//         // Add Loading UI When waiting for API res
//         // loadingUI();
//             // Render Data To UI
//         homeView.renderResults(state.home.Data, state.goToPage);
//             // Clear loading when finish render
//         // clearLoadingUI();
//         alert("Edit thanh cong")
//         await state.product.updateAPI(state.product, currentID)
//     }
   
// }
// export const addController = () => {
   
//     clearInput();
//     resetDisplayEditUI();
//     state.product = {};
//     handleProduct(inputForm.addProduct, addExecute);

// }
// export const editController =async (currentID) => {
//    if(currentID) {
//         // Handle UI
//         console.log("Tai sao lai tang")
//         state.product = new Product();
//         await state.product.getProduct(currentID);
        
        
//         inputForm.nameSelected.value = state.product.name;
//         inputForm.imageSelected.value = state.product.image;
//         inputForm.descriptionSelected.value = state.product.description;
//         inputForm.priceSelected.value = state.product.price;
//         inputForm.inventorySelected.value = state.product.inventory;
//         inputForm.ratingSelected.value = state.product.rating;
//         displayEditUI();
//         handleProduct(inputForm.editProduct,editExecute, currentID);
//     }
// }
