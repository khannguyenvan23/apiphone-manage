/* Product Control */
/***************
 * - Add product to api
 * - Edit product
 * - Del product
 */

import Product from '../model/Product.js'
import { homeController } from '../controller/homeController.js'
import * as formValid from '../model/formValid.js'
import { clearInput, clearError } from '../view/base.js'
// Global product
let product;

/* ADD PRODUCT CONTROLLER */
// Make global so easy to use onclick event
window.addController = async (event) => {
    // check valid
    event.preventDefault();
       if( formValid.checkWhenCLickEvent()){
            product = new Product();
            
            await product.addNewProduct();
            // render 
            Swal.fire({
                icon: 'success',
                title: 'Updated!',
                text: ` ${product.name} đã được thêm vào.`,
                showConfirmButton: false,
                timer: 1500
              });
            await homeController("add");
            clearInput();
            
            document.querySelector(`[data-dismiss="modal"]`).click();
       }else {
         Swal.fire({
            icon: 'error',
            title: 'Error!',
            text: 'Bạn cần nhập đầy đủ thông tin.',
            showConfirmButton: true
          });
           return;
        //    alert("Bạn cần nhập hợp lệ giá trị");
       }
            
       
    
   
    // add new 
  
}

/* EDIT PRODUCT CONTROLLER */
export const editController = async (currentID) => {
    // Add button edit 
    document.getElementsByClassName("modal-footer")[0].innerHTML = `
    <button class="btn btn-success" onclick="handleUserInputEdit(${currentID}, event)">Sửa</button>
  `;
  clearError();
    if(currentID) {
         // Make new product obj
        product = new Product();
         // Get data from API base on ID
         await product.getProduct(currentID);
         // Fill form input value make you edit easy
            product.fillForm();
         // Handle invalid input
         formValid.checkInputValue();
         
     }
 }
 // Make global so easy to use onclick event
window.handleUserInputEdit = async (currentID, event) => {
    event.preventDefault();
    if(currentID){
           // Get value from from and update to API
        if( formValid.checkWhenCLickEvent()){
            await product.updateAPI(currentID);
            // Up data home data
            
            // Render Data To UI
            await homeController("edit");
            Swal.fire({
                icon: 'success',
                title: 'Updated!',
                text: ` ${product.name} đã cập nhập thành công.`,
                showConfirmButton: false,
                timer: 1500
              });
            clearInput();
            clearError();
            document.querySelector(`[data-dismiss="modal"]`).click()
        }else {
            Swal.fire({
                icon: 'error',
                title: 'Error!',
                text: 'Bạn cần nhập đầy đủ thông tin.',
                showConfirmButton: true
              });
           return;
        }
    }
 }


 /* DELETE PRODUCT CONTROLLER */

window.deteleProductController = async (currentID) => {
    if(currentID){
        // Call Home controller delete product
        await Product.deleteProduct(currentID);
        Swal.fire({
            icon: 'success',
            title: 'Đã xóa!',
            text: ` ${product.name} đã xóa khỏi hệ thống.`,
            showConfirmButton: false,
            timer: 1500
          });
        // Render UI
        await homeController("del");
        
    }
 }