import { domString } from './base.js'


/*  Handle result */
export const clearResult = () => {
    domString.resultSelected.innerHTML = "";
}
// export const renderResult = (result) => {
//     if (!result) return
//     result.forEach(product => {
//         renderProduct(product);
//     });
// }
const renderProduct = (product) => {
     const {
         id, 
        name,
         image,
          description,
           price,
            inventory,
             rating,
            type
    } = product;
    const markup = `<tr data-productid=${id}>
						
                        <td>${id}</td>
						<td>${name}</td>
						<td><img src="${image}" alt="" width="40px" height="40px"></td>
						<td>${description}</td>
                        <td>${price}</td>
                        <td>${inventory}</td>
                        <td>${rating}</td>
                        <td>${type}</td>
						<td>
							<a href="#addEmployeeModal" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                            <a href="#deleteEmployeeModal" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                            
						</td>
					</tr>`

// <button class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>

    domString.resultSelected.insertAdjacentHTML('beforeend', markup)
}
// `<td>
// <span class="custom-checkbox">
//     <input type="checkbox" name="options[]" value="${id}">
//     <label for="checkbox1"></label>
// </span>
// </td>`
// type: 'prev' or 'next'
const createButton = (page, type) => `
   
    <li class="page-item"><a href="#${type === 'prev' ? page - 1 : page + 1}" onclick="pageController(${type === 'prev' ? page - 1 : page + 1})">${type === 'prev' ? 'Previous' : 'Next'}</a></li>
   
`;
const createPageNumber = (pages, page) =>{
    let html = '';
    for(let i = 1;i< pages + 1; i++){
        if(i === page){
            html += `<li class="page-item active"><a href="#${i}" class="page-link" onclick="pageController(${i})">${i}</a></li>`
        } else {
            html += `<li class="page-item "><a href="#${i}" class="page-link" onclick="pageController(${i})">${i}</a></li>`
        }
       
    }
    return html;
}  


const renderButtons = (page, numResults, resPerPage) => {
    const pages = Math.ceil(numResults / resPerPage);

    let button;
    if (page === 1 && pages > 1) {
        // Only button to go to next page
        button = `
        <ul class="pagination">
            ${createPageNumber(pages, page)}
            ${createButton(page, 'next')}
        </ul>
        `;
    } else if (page < pages) {
        // Both buttons
        button = `
        <ul class="pagination">
            ${createButton(page, 'prev')}
            ${createPageNumber(pages, page)}
            ${createButton(page, 'next')}
        </ul>
        `;
    } else if (page === pages && pages > 1) {
        // Only button to go to prev page
        button = `
        <ul class="pagination">
            ${createButton(page, 'prev')}
            ${createPageNumber(pages , page)}
           
         </ul>
        `;
    }
    domString.clearfix.innerHTML = ""
    domString.clearfix.insertAdjacentHTML('beforeend', button);
};

export const renderResults = (products, page = 1, resPerPage = 5) => {
    // render results of currente page
    const start = (page - 1) * resPerPage;
    const end = page * resPerPage;
    
    products.slice(start, end).forEach(renderProduct);

    // render pagination buttons
    renderButtons(page, products.length, resPerPage);
   
};
