export const domString = {
    resultSelected: document.getElementById("result"),
    clearfix: document.querySelector(".clearfix"),
    paginationSeclected: document.querySelector('.pagination')

}
/* 
 data-input="name"
 data-input="image" 
 data-input="price"
 data-input="inventory"
 data-input="rating"
 data-input="typePhone"

*/
export const inputForm = {
    contentAddEdit: document.querySelector(".contentAddEdit"),
    nameSelected: document.querySelector('[data-input="name"]'),
    imageSelected: document.querySelector('[data-input="image"]'),
    descriptionSelected: document.querySelector('[data-input="description"]'),
    priceSelected: document.querySelector('[data-input="price"]'),
    inventorySelected: document.querySelector('[data-input="inventory"]'),
    ratingSelected: document.querySelector('[data-input="rating"]'),
    typePhoneSelected: document.querySelector('[data-input="typePhone"]'),
    addProduct :document.getElementById('addproduct'),
    editProduct :document.getElementById('editproduct')
}
export const inputError = {
    errorName: document.getElementById("errorName"),
    errorImage: document.getElementById("errorImage"),
    errorDescription: document.getElementById("errorDescription"),
    errorPrice: document.getElementById("errorPrice"),
    errorInventory: document.getElementById("errorInventory"),
    errorRating: document.getElementById("errorRating"),
    errorTypePhone: document.getElementById("errorTypePhone"),
}


const globalString = {
    spinner: "lds-facebook",
}
// change display input 

export const displayEditUI = () => {
    inputForm.contentAddEdit.textContent = "Sửa thông tin điện thoại",
    inputForm.addProduct.style.display = "none";
    inputForm.editProduct.style.display = "block";
}
// export const resetDisplayEditUI = () => {
//     inputForm.contentAddEdit.textContent = "Thêm thông tin điện thoại"
//     inputForm.addProduct.style.display = "block";
//     inputForm.editProduct.style.display = "none";
// }
export const clearInput = () => {
    inputForm.nameSelected.value = "";
    inputForm.imageSelected.value = "";
    inputForm.descriptionSelected.value = "";
    inputForm.priceSelected.value = "";
    inputForm.inventorySelected.value = "";
    inputForm.ratingSelected.value = "";
    inputForm.typePhoneSelected[0].selected = true;
}
export const clearError = () => {
    inputError.errorName.textContent = "";
    inputError.errorImage.textContent = ""; 
    inputError.errorDescription.textContent = "";
    inputError.errorPrice.textContent = "";
    inputError.errorInventory.textContent = "";
    inputError.errorRating.textContent = "";
    inputError.errorTypePhone.textContent = "";
}
/* Loading spinner */

export const loadingUI = () => {
    const markup = `<div class="${globalString.spinner}"><div></div><div></div><div></div></div>`
    domString.resultSelected.insertAdjacentHTML('beforeend', markup);
}
export const clearLoadingUI = () => {
    const loader = document.querySelector(`.${globalString.spinner}`);
    if(loader) loader.parentElement.removeChild(loader);
    
}