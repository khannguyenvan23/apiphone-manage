#demoo https://khannguyenvanapictr.netlify.app/

Phần 2: (tạo 1 source code riêng để làm) Phần quản trị

1.Thực hiện các chức năng : thêm xoá sửa sản phẩm (tương tự thêm xoá sửa nhân viên với các api dưới)

API products:

 - GET https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products : lấy danh sách products

 - GET https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id} : lấy thông tin 1 product theo id

   VD: product có id là 123 => api là:  https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/123 .Ở dưới cũng tương tự nha

 - POST https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products: Thêm product

 - PUT https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id} : cập nhật product theo id

 - DELETE https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id} : Xoá product theo id